#!/bin/env bash

#///////////////////////#
#       O M E G A       #
#///////////////////////#
#   I N S T A L L E R   #
#///////////////////////#

# Simple installer for my 'Omega Desktop' configs.

set -e

# Print welcome & title
echo -e "\033[32m  _____                 _             _   _       "
echo -e " |_   _|               | |           | | | |              "
echo -e "   | |    _ __    ___  | |_    __ _  | | | |   ___   _ __ "
echo -e "   | |   | |_ \  / __| | __|  / _| | | | | |  / _ \ | |__|"
echo -e "  _| |_  | | | | \__ \ | |_  | (_| | | | | | |  __/ | |   "
echo -e " |_____| |_| |_| |___/  \__|  \__,_| |_| |_|  \___| |_|   \e[0m"


echo " "
echo "Welcome $USER!" && sleep 2

# System update
echo "Doing a system update..." && sleep 2
sudo pacman --noconfirm -Syu

# Installing packages
echo "installing required packages..." && sleep 2
sudo pacman -Syu --needed --noconfirm - < packages-repository.txt

# Clean the terminal
clear

# Backup & copy the files to the corresponding path
echo "Copying your configs to the correct path...." && sleep 2

# FONTS
    if [ -d ~/.local/share/fonts/IosevkaTermNerdFontComplete.ttf ]; then
        echo "Iosevka Term font detected, ok..."
	      cp -R ./fonts/* ~/.local/share/fonts; 
    else
        echo "Installing Iosevka Term font..."
        mkdir ~/.local/share/fonts && cp -R ./fonts/* ~/.local/share/fonts;
    fi
    sleep 2

# CONFIG FOLDER 
mkdir -p ~/.config/

# SCRIPTS
    if [ -d ~/.config/scripts ]; then
        echo "Scripts dir detected, backing up..."
        mkdir ~/.config/scripts.old && mv ~/.config/scripts/* ~/.config/scripts.old/
        cp -r ./config/scripts/* ~/.config/scripts;
    else
        echo "Installing script configs..."
        mkdir ~/.config/scripts && cp -r ./config/scripts/* ~/.config/scripts;
    fi
    sleep 2

# ROFI
    if [ -d ~/.config/rofi ]; then
        echo "Rofi configs detected, backing up..."
        mkdir ~/.config/rofi.old && mv ~/.config/rofi/* ~/.config/rofi.old/
        cp -r ./config/rofi/* ~/.config/rofi;
    else
        echo "Installing rofi configs..."
        mkdir ~/.config/rofi && cp -r ./config/rofi/* ~/.config/rofi;
    fi
    sleep 2

# THEME
    if [ -d ~/.themes ]; then
        echo ".themes dir detected, backing up..."
        mkdir ~/.themes.old && mv ~/.themes/* ~/.themes.old/
        cp -r ./themes/* ~/.themes;
    else
        echo "Installing theme configs..."
        mkdir ~/.themes && cp -r ./themes/* ~/.themes;
    fi
    sleep 2

# ICONS
    if [ -d ~/.icons ]; then
        echo ".icons dir detected, backing up..."
        mkdir ~/.icons.old && mv ~/.icons/* ~/.icons.old/
        cp -r ./icons/* ~/.icons;
    else
        echo "Installing icons configs..."
        mkdir ~/.icons && cp -r ./icons/* ~/.icons;
    fi
    sleep 2

# GTK
    if [ -d ~/.config/gtk-3.0 ]; then
        echo "gtk dir detected, backing up..."
        mkdir ~/.config/gtk-3.0.old && mv ~/.config/gtk-3.0/* ~/.config/gtk-3.0.old/
        cp -r ./config/gtk-3.0/* ~/.config/gtk-3.0;
    else
        echo "Installing gtk configs..."
        mkdir ~/.config/gtk-3.0 && cp -r ./config/gtk-3.0/* ~/.config/gtk-3.0;
    fi
    sleep 2

# BSPWM
    if [ -d ~/.config/bspwm ]; then
        echo "Bspwm configs detected, backing up..."
        mkdir ~/.config/bspwm.old && mv ~/.config/bspwm/* ~/.config/bspwm.old/
        cp -r ./config/bspwm/* ~/.config/bspwm;
    else
        echo "Installing bspwm configs..."
        mkdir ~/.config/bspwm && cp -r ./config/bspwm/* ~/.config/bspwm;
    fi
    sleep 2

# SXHKD
    if [ -d ~/.config/sxhkd ]; then
        echo "Sxhkd configs detected, backing up..."
        mkdir ~/.config/sxhkd.old && mv ~/.config/sxhkd/* ~/.config/sxhkd.old/
        cp -r ./config/sxhkd/* ~/.config/sxhkd;
    else
        echo "Installing sxhkd configs.."
        mkdir ~/.config/sxhkd && cp -r ./config/sxhkd/* ~/.config/sxhkd;
    fi
    sleep 2

# POLYBAR
    if [ -d ~/.config/polybar ]; then
        echo "Polybar configs detected, backing up..."
        mkdir ~/.config/polybar.old && mv ~/.config/polybar/* ~/.config/polybar.old/
        cp -r ./config/polybar/* ~/.config/polybar;
    else
        echo "Installing polybar configs..."
        mkdir ~/.config/polybar && cp -r ./config/polybar/* ~/.config/polybar;
    fi
    sleep 2

# PICOM
    if [ -d ~/.config/picom ]; then
        echo "Picom configs detected, backing up..."
        mkdir ~/.config/picom.old && mv ~/.config/picom/* ~/.config/picom.old/
        cp -r ./config/picom/* ~/.config/picom;
    else
        echo "Installing picom configs..."
        mkdir ~/.config/picom && cp -r ./config/picom/* ~/.config/picom;
    fi
    sleep 2

# ALACRITTY
    if [ -d ~/.config/alacritty ]; then
        echo "Alacritty configs detected, backing up..."
        mkdir ~/.config/alacritty.old && mv ~/.config/alacritty/* ~/.config/alacritty.old/
        cp -r ./config/alacritty/* ~/.config/alacritty;
    else
        echo "Installing alacritty configs..."
        mkdir ~/.config/alacritty && cp -r ./config/alacritty/* ~/.config/alacritty;
    fi
    sleep 2

# WALLPAPERS
    if [ -d /usr/share/wallpapers ]; then
        echo "Adding wallpapers to ~/wallpapers..."
        sudo cp ./wallpapers/* /usr/share/wallpapers/;
    else
        echo "Installing wallpapers..."
        sudo mkdir /usr/share/wallpapers && sudo cp -r ./wallpapers/* /usr/share/wallpapers/;
    fi
    sleep 2

# NITROGEN
    if [ -d ~/.config/nitrogen ]; then
        echo "Nitrogen configs detected, backing up..."
        mkdir ~/.config/nitrogen.old && mv ~/.config/nitrogen/* ~/.config/nitrogen.old/
        cp -r ./config/nitrogen/* ~/.config/nitrogen;
    else
        echo "Installing qtile configs..."
        mkdir ~/.config/nitrogen && cp -r ./config/nitrogen/* ~/.config/nitrogen;
    fi
    sleep 2

# BASH ALIASES
    if [ -d ~/.bashrc-personal ]; then
        echo "Bash aliases detected, backing up..."
        mkdir ~/.config/bash.old && mv ~/.bashrc-personal ~/.config/bash.old/
        cp -r ./bash/.bashrc-personal ~/;
    else
        echo "Installing bash configs..."
        cp -r ./bash/.bashrc-personal ~/;
    fi
    sleep 2

# ZSH ALIASES 
    if [ -d ~/.zshrc-personal ]; then
        echo "Zsh aliases detected, backing up..."
        mkdir ~/.config/zsh.old && mv ~/.zshrc-personal ~/.config/zsh.old/
        cp -r ./bash/.zshrc-personal ~/;
    else
        echo "Installing zsh configs..."
        cp -r ./bash/.zshrc-personal ~/;
    fi
    sleep 2

# DUNST
    if [ -d ~/.config/dunst ]; then
        echo "Dunst configs detected, backing up..."
        mkdir ~/.config/dunst.old && mv ~/.config/dunst/* ~/.config/dunst.old/
        cp -r ./config/dunst/* ~/.config/dunst;
    else
        echo "Installing dunst configs..."
        mkdir ~/.config/dunst && cp -r ./config/dunst/* ~/.config/dunst;
    fi
    sleep 2

# NEOFETCH
    if [ -d ~/.config/neofetch ]; then
        echo "Neofetch configs detected, backing up..."
        mkdir ~/.config/neofetch.old && mv ~/.config/neofetch/* ~/.config/neofetch.old/
        cp -r ./config/neofetch/* ~/.config/neofetch;
    else
        echo "Installing neofetch configs..."
        mkdir ~/.config/neofetch && cp -r ./config/neofetch/* ~/.config/neofetch;
    fi
    sleep 2

# STARSHIP
    if [ -d ~/.config/starship.toml ]; then
        echo "Starship configs detected, backing up..."
        mkdir ~/.config/starship.old && mv ~/.config/starship.toml ~/.config/starship.old/
        cp -r ./starship/* ~/.config;
    else
        echo "Installing starship configs..."
        cp -r ./starship/* ~/.config;
    fi
    sleep 2

# Adding permissions
echo "Adding permissions..."
chmod -R +x ~/.config/bspwm/scripts
chmod -R +x ~/.config/polybar/scripts
chmod -R +x ~/.config/scripts

# Reload fonts cache
sudo fc-cache -f -v

# Clean the terminal
clear

# Adding bash aliases to ~/.bashrc
echo "[[ -f ~/.bashrc-personal ]] && . ~/.bashrc-personal" >> ~/.bashrc

# Adding zsh aliases to ~/.zshrc
echo "[[ -f ~/.zshrc-personal ]] && . ~/.zshrc-personal" >> ~/.zshrc

# Print last instructions
echo -e "TO LAUNCH, use your Display Manager (ie. lightdm or sddm, etc.) and select \033[32mbspwm\e[0m"
echo " "
echo "To see the keybindings:" 
echo "1) Open a terminal 'Super + Enter'" 
echo -e "2) Type \033[32m'keybindindings' \e[0m" 
echo "3) Press Enter"
echo " "
echo -e "\033[32mFIN\e[0m"
echo " "

#---------------------------------------------------#
# Author: Axarva                                    #
# Source: https://github.com/Axarva/dotfiles-2.0    #
# Editor: Brandon Puc                               #
#---------------------------------------------------#
