<img src="assets/omega-long.png">

# Welcome to my dotfiles

Hey! I'm Brandon Puc and this is my personal backup.        
Do you need any of this configs? Well, go to your terminal & `git clone`

# Table of contents
- [Overview](#overview)
- [Screenshots](#screenshots)
- [Installation](#installation)
- [Keybindings](#keybindings)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)

> **TODO:** Add spectrwm and qtile

# Overview
- Device: `Laptop`
- Operating System: `ArcoLinux`
- Window Manager: `Bspwm`
- Launcher: `Rofi`/`Dmenu` 
- Notifications: `Dunst`
- Terminal: `Alacritty`
- File mananger: `Ranger`/`Thunar`
- Editor: `neovim`
- Web Browser: `Firefox`/`Brave`
- Music Player: `Mpv`
- Wallpapers: `Nitrogen`

# Screenshots
Here are some screenshots of my finished configs.

## Bspwm
![DesktopB.png](assets/DesktopB.png)
![Launcher.png](assets/Launcher.png)

# Installation
     
Open a terminal and...

**1. Clone my configs**
```bash
git clone https://gitlab.com/bpuc/dotfiles.git
```
**2. cd into dotfiles**
```bash
cd dotfiles
```
**3. Make the script executable**
```bash
chmod +x installer.sh
```
**4. Run the script**
```bash
./installer.sh
```
> Before installing it's important that you make a backup of your files. 

## Note:

Note that this is a personal post install script created to work on Arcolinux and may not work on other distros due to the repository packages that are installed by default. 

ArcoLinux isn't my distro, what can I do?

1. If you are using an arch based distro, the [Arcolinux spices](https://www.arcolinux.info/arcolinux-spices-application/) can help you obtain ArcoLinux packages from the ArcoLinux repositories.

2. If you are using a non-Arch distro, I recommend that you copy the files manually and check the `packages-repository.txt` for dependencies.

## Keybindings
Here are some essential keybindings


|    Keybind    |  Application  |      Function     |
| ------------- | ------------- | ----------------- |
|`Super + Enter`| Alacritty     | Open Terminal     |
| `Super + d`   | Rofi          | Run Launcher      |
| `Super + n`   | Thunar        | Open Filemananger |
| `Super + x`   | Powermenu     | Display Powermenu |
| `Super + w`   | Brave         | Open Browser      |
| `Alt + 1`     | Configmenu    | Open Config Files |
| `Alt + 3`     | Rofi          | Wifimenu          |
| `Alt + 5`     | Rofi          | Todo app          |
| `Alt + Shift + 5`   | Rofi    | Notes app         | 
| `Print`       | Rofi          | Take Screenshot   | 
| `Super + q`         |  wm | Close Current Window  |
| `Super + Alt + r` |  wm | Reload Configs        |

> To see all keybindings go to `sxhkdrc`. Press "`Alt + 1`" and choose the sxhkd option.

## Contributing
Comments and suggestions are welcome

## Credits
Many lines in my configurations and scripts are by inspiration or modification of original projects created by other people. Thanks to all of them for their great contribution.

- [DT](https://gitlab.com/dwt1/dotfiles)
- [Axarva](https://github.com/Axarva/dotfiles-2.0)
- [TechnicalDC](https://github.com/TechnicalDC)
- [EOS Bspwm community](https://github.com/EndeavourOS-Community-Editions/bspwm)
- [r/unixporn community](https://www.reddit.com/r/unixporn/)

## License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
