#!/bin/env bash

#///////////////////////#
#    A U T O P U S H    #
#///////////////////////#
#     S R C R I P T     #
#///////////////////////#

# Script simple para agilizar el proceso de enviar mi repo a gitlab
# send to /usr/local/bin/

git add --all

echo "Enter your message"
read message
git commit -m "${message}"

git push

echo "//// Listo... ////"
# fin
