#!/usr/bin/env bash

# Check Internet connectivity
if ping -q -c 1 -W 1 archlinux.org > /dev/null 2>&1; then
    # If there is Internet connection, get updates
    updates=$(checkupdates)

    if [ -z "$updates" ]; then
        count=0
    else
        printf '%s\n' "$updates" > /tmp/bspwm-arch-updates
        count=$(wc -l "/tmp/bspwm-arch-updates" | awk '{print $1}')
    fi

    echo "%{T1}%{T-} %{T2}$count%{T-}"
else
    # If there is no Internet connection, print a no internet symbol
    echo "%{T1}年%{T-} %{T2}?%{T-}"
fi

# Author: EOS Bspwm comunity
# Editor: Brandon Puc
