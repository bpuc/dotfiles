#!/bin/bash

# List of phrases
frases=(
    "Hola $USER, bienvenido de nuevo."
    "Buen día $USER, es un gusto tenerte de vuelta."
    "Hey $USER, ¿listo para trabajar?"
    "Bienvenido $USER."
    "Hola $USER, es un gusto verte de nuevo."
)

# Function to display a random phrase
mostrar_frase_aleatoria() {
    # Get a random index
    indice=$(( RANDOM % ${#frases[@]} ))
    
    # Get the random phrase
    frase="${frases[$indice]}"
    
    # Sent notification
    dunstify -u normal "OMEGA" "$frase"
}

# Display a random phrase when the script is executed
mostrar_frase_aleatoria

