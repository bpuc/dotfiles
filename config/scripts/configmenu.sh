#!/bin/bash

#///////////////////////#
#      C O N F I G      #
#///////////////////////#
#        M E N U        #
#///////////////////////#      

# Replace nvim with the editor of your choice
MYEDITOR="alacritty -e nvim" 

declare -A options
options[ neofetch]="$HOME/.config/neofetch/config.conf"
options[󰑣 rofi]="$HOME/.config/rofi/config.rasi"
options[ spectrwm]="$HOME/.config/spectrwm/spectrwm.conf"
options[ bspwm]="$HOME/.config/bspwm/bspwmrc"
options[ qtile]="$HOME/.config/qtile/config.py"
options[ alacritty]="$HOME/.config/alacritty/alacritty.toml"
options[ baraction]="$HOME/.config/spectrwm/scripts/baraction.sh"
options[󱟛 polybar]="$HOME/.config/polybar/config.ini"
options[ bash]="$HOME/.bashrc-personal"
options[ zsh]="$HOME/.zshrc-personal"
options[󰵙 dunst]="$HOME/.config/dunst/dunstrc"
options[󰌌 sxhkd]="$HOME/.config/sxhkd/sxhkdrc"
options[ picom]="$HOME/.config/picom/picom.conf"
options[󱓞 starship]="$HOME/.config/starship.toml"
options[󰢱 chadrc]="$HOME/.config/nvim/lua/custom/chadrc.lua"
##options[]="-"

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
choice=$(printf '%s\n' "${!options[@]}" | rofi -dmenu -i -show-icons -line 10 \
	                                  -p "Edit" -font "Iosevka Term 13") # -no-sidebar-mode

# What to do when/if we choose a file to edit.
if [ "$choice" ]; then
	conf=$(printf '%s\n' "${options["${choice}"]}")
	$MYEDITOR "$conf"
# What to do if we just escape without choosing anything.
else
    echo "Program terminated." && exit 0
fi

#------------------------------------#
# editor: Brandon Puc                #
# Author: Dilip Chauhan	             #
# Github: https://github/TechnicalDC #
#------------------------------------# 
