#!/bin/bash

#///////////////////////#
#      S I M P L E      #
#///////////////////////#
#      L E D - O N      #
#///////////////////////#

# Options to be displayed
option0=" - on"
option1=" - off"

# Options to be displyed
options="$option0\n$option1"

selected="$(echo -e "$options" | rofi -lines 2 -dmenu -font "Iosevka Term 13" -p "led")"
case $selected in
    $option0)
        xset led on;;
    $option1)
        xset led off;;
esac

#---------------------#
# Author: Brandon Puc #
# --------------------#
