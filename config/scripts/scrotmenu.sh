#!/bin/bash
                       
#///////////////////////#
#       S C R O T       #
#///////////////////////#
#        M E N U        #
#///////////////////////#

# Creates the dir
FOLDER="$HOME/scrot/"

if [[ ! -d "${FOLDER}" ]]; then
    mkdir -p "$FOLDER"
fi

# This script requires scrot 
option1=" - fullscreen"
option2=" - fullscreen + delay"
option3="缾 - focused window"
option4=" - select area"

options="$option1\n$option2\n$option3\n$option4"

choice=$(echo -e "$options" | rofi -i -dmenu -show-icons -font "Iosevka Term 13" -no-sidebar-mode -lines 4 -width 20 -p "scrot") 

case $choice in
	$option1)
		scrot -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved' -i 'dialog-information' -t 2000  ;; 
	$option2)
		delayoption1="+ 3 sec"
		delayoption2="+ 5 sec"
		delayoption3="+ 10 sec"
		delayoptions="$delayoption1\n$delayoption2\n$delayoption3"
		delay=$(echo -e "$delayoptions" | rofi -i -dmenu -no-show-icons -font "Iosevka Term 13" -no-sidebar-mode -lines 3 -width 20 -p "delay") 

		case $delay in

			$delayoption1)
				scrot -d 3 -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved' -i 'dialog-information' -t 2000 ;; 
			$delayoption2)
				scrot -d 5 -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved' -i 'dialog-information' -t 2000 ;; 
			$delayoption3)
				scrot -d 10 -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved' -i 'dialog-information' -t 2000 ;; 
		esac ;;

	$option3)
		scrot -u -b -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved' -i 'dialog-information' -t 2000 ;;
	$option4)
		scrot -s -e 'mv $f ~/scrot/' && notify-send -a 'Scrot' '  screenshot saved.' -i 'dialog-information' -t 2000 ;;

esac
#------------------------------------#
# Original by: Dilip Chauhan         #
# Github: https://github/TechnicalDC #
#------------------------------------#
