#!/usr/bin/env bash

# Option 1
dmenu_run -b -p 'Search:' -nb '#1d2021' -sf '#1d2021' -sb '#d4be98' -nf '#d4be98' -fn 'Iosevka Term-12'
 
# Option 2
#  dmenu_run -p 'Search:' -nb '#1d2021' -sf '#1d2021' -sb '#d4be98' -nf '#d4be98' -fn 'Iosevka Term-12'
 
# Option 3
# dmenu_run -p ' ' -nb '#1d2021' -sf '#1d2021' -sb '#d4be98' -nf '#d4be98' -l '3' -fn 'Iosevka Term-12'

# Author: Brandon Puc
