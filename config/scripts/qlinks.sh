#!/usr/bin/sh

#///////////////////////#
#       Q U I C K       #
#///////////////////////#
#       L I N K S       #
#///////////////////////#

BROWSER="brave"

declare -A options 

options[ youtube]="https://youtube.com"
options[ odysee]="https://odysee.com"
options[ reddit]="https://reddit.com"
options[ r/unixporn]="https://reddit.com/r/unixporn"
options[ gitlab]="https://gitlab.com"
options[ itsfoss]="https://news.itsfoss.com/"
options[ google]="https://google.com"
options[ soundcloud]="https://soundcloud.com"
#options[]=""
#options[quit]="quit"


choice=$(printf '%s\n' "${!options[@]}" | rofi -dmenu -i -l 10 -p 'qlinks' -font "Iosevka Term 13")


if [[ "$choice" == quit ]]; then
	echo "Program Terminated." && exit 1
elif [ "$choice" ]; then
	cfg=$(printf '%s\n' "${options["${choice}"]}" | awk '{print $NF}')
	$BROWSER "$cfg"
else
	echo "Program Terminated." && exit 1
fi

